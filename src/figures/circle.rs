use crate::Colors;
use crate::Figure;

// pub enum Point {
//     x(usize),
//     y(usize),
// }

#[derive(Debug)]
pub struct Circle {
    pub radius: usize,
    pub box_width: usize,
    // pub center: Point,
    pub pixels: Vec<Colors>,
}

impl Circle {
    pub fn new(radius: usize) -> Circle {
        let width = (2 * radius) + 1;

        let mut pixels = vec![];

        for i in 0..width.pow(2) {
            let x = i % width;
            let y = i / width;

            if is_in_distance(&x, &y, &radius) {
                pixels.push(Colors {
                    red: 0,
                    green: 255,
                    blue: 0,
                });
            } else {
                pixels.push(Colors {
                    red: 255,
                    green: 255,
                    blue: 255,
                });
            }
        }

        Circle {
            radius: radius,
            box_width: width,
            pixels: pixels,
        }
    }

    pub fn to_figure(&self, canvas: &Figure) -> Result<Figure, &'static str> {
        if self.box_width > canvas.width || self.box_width > canvas.height {
            return Err("Circle doesn't fit in the canvas");
        }

        let mut pixels: Vec<Colors> = vec![];
        let mut posx = 0;
        let mut posy = 0;

        let mut fig_pixels_iter = self.pixels.iter();

        for canv_pixel in canvas.pixels.iter() {
            if posx < self.box_width && posy < self.box_width {
                match fig_pixels_iter.next() {
                    Some(fig_pixel) => pixels.push(fig_pixel.clone()),
                    None => return Err("No pixel found in the figure"),
                };
            } else {
                pixels.push(canv_pixel.clone());
            }
            posx += 1;

            if posx > 0 && posx % canvas.width == 0 {
                posx = 0;
                posy += 1;
            }
        }

        Ok(Figure {
            width: canvas.width,
            height: canvas.height,
            pixels: pixels,
        })
    }
}

fn is_in_distance(x: &usize, y: &usize, radius: &usize) -> bool {
    let a_sq = (radius.clone() as i32 - x.clone() as i32).pow(2);
    let b_sq = (radius.clone() as i32 - y.clone() as i32).pow(2);
    let c = a_sq + b_sq;
    (c as f64).sqrt() < radius.clone() as f64
}
