use crate::Colors;
use crate::Figure;

#[derive(Debug)]
pub struct Rectangle {
    pub width: usize,
    pub height: usize,
    pub pixels: Vec<Colors>,
}

impl Rectangle {
    pub fn new(width: usize, height: usize) -> Rectangle {
        let pixels = vec![Colors::black(); width * height];

        Rectangle {
            width,
            height,
            pixels,
        }
    }

    #[allow(dead_code)]
    pub fn area(&self) -> usize {
        self.width * self.height
    }

    pub fn new_color(&self, red: u8, green: u8, blue: u8) -> Rectangle {
        let mut pixels = vec![];

        for mut color in self.pixels.clone() {
            color.red = red;
            color.green = green;
            color.blue = blue;

            pixels.push(color);
        }

        let width = self.width;
        let height = self.height;

        Rectangle {
            width,
            height,
            pixels,
        }
    }

    pub fn to_figure(&self, canvas: &Figure) -> Result<Figure, &'static str> {
        if self.width > canvas.width || self.height > canvas.height {
            return Err("Rectangle doesn't fit in the canvas");
        }

        let mut pixels: Vec<Colors> = vec![];
        let mut posx = 0;
        let mut posy = 0;
        let mut fig_pixel_iter = self.pixels.iter();

        for canv_pixel in canvas.pixels.iter() {
            if posx < self.width && posy < self.height {
                match fig_pixel_iter.next() {
                    Some(fig_pixel) => pixels.push(fig_pixel.clone()),
                    None => return Err("No pixel found in the figure"),
                };
            } else {
                pixels.push(canv_pixel.clone());
            }
            posx += 1;

            if posx > 0 && posx % canvas.width == 0 {
                posx = 0;
                posy += 1;
            }
        }

        Ok(Figure {
            width: canvas.width,
            height: canvas.height,
            pixels: pixels,
        })
    }
}
