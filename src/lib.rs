use std::error::Error;
use std::fs;

mod figures;
pub use crate::figures::rectangle::Rectangle;
pub use crate::figures::circle::Circle;

#[derive(Debug, Clone)]
pub struct Colors {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl Colors {
    fn black() -> Colors {
        // Retunrs black
        Colors {
            red: 0,
            green: 0,
            blue: 0,
        }
    }
}

pub struct Figure {
    width: usize,
    height: usize,
    pixels: Vec<Colors>,
}

impl Figure {
    pub fn new(width: usize, height: usize) -> Figure {
        let color_pix = Colors{red: 255, green: 255, blue: 255};
        let pixels = vec![color_pix; width * height];

        Figure {
            width,
            height,
            pixels,
        }
    }
}

pub fn save_as_ppm_gen(fig: &Figure, name: &str) -> Result<(), Box<dyn Error>> {
    let header = format!(
        "P3\n{} {}\n255\n",
        fig.width.to_string(),
        fig.height.to_string()
    );

    let mut pixels = vec![];
    for pixel in &fig.pixels {
        let pixel = format!("{} {} {}\n", pixel.red, pixel.green, pixel.blue);
        pixels.push(pixel);
    }

    let pixels = pixels.concat();

    let file_contents = header + &pixels;

    fs::write(name, file_contents)?;

    Ok(())
}
