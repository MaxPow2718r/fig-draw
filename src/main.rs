use fig_draw::Rectangle;
use fig_draw::Circle;
use std::process;

fn main() {
    let canvas = fig_draw::Figure::new(1280, 720);
    let rect = Rectangle::new(720, 480);
    let rect = rect.new_color(255, 0, 0);

    let circle = Circle::new(300);

    match rect.to_figure(&canvas) {
        Ok(rect) => {
            if let Err(e) = fig_draw::save_as_ppm_gen(&rect, "rect.ppm") {
                eprintln!("Problem in the runtime {}", e);
                process::exit(1);
            }
        }
        Err(e) => {
            eprintln!("Problem in the runtime {}", e);
            process::exit(1);
        }
    }

    match circle.to_figure(&canvas) {
        Ok(circle) => {
            if let Err(e) = fig_draw::save_as_ppm_gen(&circle, "circle.ppm") {
                eprintln!("Problem in the runtime {}", e);
                process::exit(1);
            }
        }
        Err(e) => {
            eprintln!("Problem in the runtime {}", e);
            process::exit(1);
        }
    }
}
